<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model{

	protected $table = 'post';
    
    protected $dates = ['deleted_at', 'created_at', 'updated_at'];
	protected $fillable = ['user_id', 'title', 'body'];
	protected $guarded = [];
	protected $hidden = [];

}
