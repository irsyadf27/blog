<?php
namespace App\Http\Controllers;
use Auth;
use Input;
use App\Post;
class AdminController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| Home Controller
	|--------------------------------------------------------------------------
	|
	| This controller renders your application's "dashboard" for users that
	| are authenticated. Of course, you are free to change or remove the
	| controller as you wish. It is just here to get your app started!
	|
	*/

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct(){

	}

	/* Add post */
	public function getAddpost(){
		return view('admin.addpost');
	}

	public function postAddpost(){
		$title = Input::get('title') ? Input::get('title') : date('Y-m-d H:i:s');
		$body = Input::get('body');
		$save = new Post;
		$save->user_id = Auth::user()->id;
		$save->title = $title;
		$save->body = $body;
		$save->slug = str_slug($title, '-');
		$save->save();
		return redirect('/administraktor/index');
	}

	/* Edit Post */
	public function getEditpost($id){
		$find = Post::find($id);
		return view('admin.editpost', array('data' => $find));
	}

	public function postEditpost(){
		$id = Input::get('id_post');
		$title = Input::get('title');
		$body = Input::get('body');
		$update = Post::where('id', $id)->first();
		$update->user_id = Auth::user()->id;
		$update->title = $title;
		$update->body = $body;
		$update->slug = str_slug($title, '-');
		$update->save();
		return redirect('/administraktor/index');
	}

	public function getDeletepost($id){
		$find = Post::find($id);
		if($find->delete()){
			return redirect('/administraktor/index');
		} else {
			return "Error";
		}
	}
	/**
	 * Show the application dashboard to the user.
	 *
	 * @return Response
	 */
	public function getIndex(){
		$data = Post::paginate(10);
		return view('admin.index', array('data' => $data));
	}



}
