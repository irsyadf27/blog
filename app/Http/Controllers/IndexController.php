<?php
namespace App\Http\Controllers;
use App\Post;
class IndexController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| Home Controller
	|--------------------------------------------------------------------------
	|
	| This controller renders your application's "dashboard" for users that
	| are authenticated. Of course, you are free to change or remove the
	| controller as you wish. It is just here to get your app started!
	|
	*/

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct(){

	}

	/**
	 * Show the application dashboard to the user.
	 *
	 * @return Response
	 */
	public function index(){
		$data = Post::paginate(1);
		return view('index', array('data' => $data));
	}

	public function read($id, $slug){
		$d = Post::where('id', $id)->where('slug', $slug)->first();
		return view('read', array('d' => $d));
	}

}
