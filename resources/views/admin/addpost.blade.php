@extends('app')

@section('content')
<div class="container-fluid">
	<div class="row">
		<div class="col-md-10 col-md-offset-1">
			<div class="panel panel-default">
				<div class="panel-heading">Add Post</div>
				<div class="panel-body">
					<form class="form-horizontal" role="form" action="/administraktor/addpost" method="post">
						<input type="hidden" name="_token" value="{{ csrf_token() }}">
					  <div class="form-group">
					    <label for="inputTitle" class="col-sm-2 control-label">Title</label>
					    <div class="col-sm-10">
					      <input type="text" class="form-control" id="inputTitle" placeholder="Title" name="title">
					    </div>
					  </div>
					  <div class="form-group">
					    <label for="inputPassword3" class="col-sm-2 control-label">Body</label>
					    <div class="col-sm-10">
					      <textarea class="form-control" rows="3" name="body"></textarea>
					    </div>
					  </div>

					  <div class="form-group">
					    <div class="col-sm-offset-2 col-sm-10">
					      <button type="submit" class="btn btn-default">Post</button>
					    </div>
					  </div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
