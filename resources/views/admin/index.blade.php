@extends('app')

@section('content')
<div class="container-fluid">
	<div class="row">
		<div class="col-md-10 col-md-offset-1">
			<div class="panel panel-default">
				<div class="panel-heading">Posting</div>
				<div class="panel-body">
					<a class="btn btn-primary" href="/administraktor/addpost">Add post</a>
					<table class="table table-striped table-hover">
						<caption>List Post</caption>
						<tr>
							<td width="80%">Title</td>
							<td>Action</td>
						</tr>
						@foreach($data as $d)
						<tr>
							<td><a href="#">{{ str_limit($d->title, 50) }}</a></td>
							<td>
								<div class="btn-group" role="group" aria-label="...">
								  <a class="btn btn-sm btn-primary" href="/administraktor/editpost/{{ $d->id }}">Edit</a>
								  <a class="btn btn-sm btn-danger" href="/administraktor/deletepost/{{ $d->id }}">Delete</a>	
								</div>
							</td>
						</tr>
						@endforeach
					</table>
					{!! $data->render() !!}
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
