<!DOCTYPE html>
<html>
<head>
  <!--Import Google Icon Font-->
  <link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
  <!--Import materialize.css-->
  <link type="text/css" rel="stylesheet" href="{{ asset('/assets/css/materialize.min.css') }}"  media="screen,projection"/>
  <link type="text/css" rel="stylesheet" href="{{ asset('/assets/css/style.css') }}"/>
  <title>-</title>
  <!--Let browser know website is optimized for mobile-->
  <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
</head>
<body>
  <nav>
    <div class="nav-wrapper grey darken-3">
      <a href="#" class="brand-logo center">;</a>
      <ul id="nav-mobile" class="left hide-on-med-and-down">
        <li><a href="{{ url('/') }}">Home</a></li>
        <li><a href="#">Contact</a></li>
      </ul>
    </div>
  </nav>
  <div class="intro z-depth-1">
    <h1 class="grey-text text-lighten-5"><a href="{{ url('/') }}"><img src="{{ Avatar::create('Irsyad Fauzan')->toBase64() }}" /></a></h1>
    <h5 class="grey lighten-4 grey-text text-darken-1">Just A Stupid People.</h5>
  </div>
  <div class="row">
    <div class="col s12 m9">
      <div class="card white darken-1">
        <div class="card-content post-content">
          <div class="card-title blue-text post-title">{{ $d->title }}</div>
          <p>{!! $d->body !!}</p>
        </div>
        <div class="card-action">
          
          <i class="tiny material-icons">schedule</i> <span class="post-info">28 October 2015 9:15</span>
        </div>
      </div>
      <ul class="pagination">
        <li class="disabled"><a href="#!"><i class="material-icons">chevron_left</i></a></li>
        <li class="active"><a href="#!">1</a></li>
        <li class="waves-effect"><a href="#!">2</a></li>
        <li class="waves-effect"><a href="#!">3</a></li>
        <li class="waves-effect"><a href="#!">4</a></li>
        <li class="waves-effect"><a href="#!">5</a></li>
        <li class="waves-effect"><a href="#!"><i class="material-icons">chevron_right</i></a></li>
      </ul>
    </div>
    <div class="col s12 m3 about">
        <div class="card-panel">
    <form class="col s12">
      <div class="row">
        <div class="input-field col s12">
          <i class="material-icons prefix">search</i>
          <input id="icon_prefix" type="text">
          <label for="icon_prefix">Search...</label>
        </div>
        <div class="col s4 offset-s6">
          <button class="btn waves-effect waves-light" type="submit" name="action">Search
          </button>
        </div>
      </div>
    </form>
    asdasdas
        </div>
    </div>
  </div>

  <footer class="page-footer blue-grey darken-3">
    <div class="container">
      <div class="row">
        <div class="col l6 s12">
          <h5 class="white-text">Footer Content</h5>
          <p class="grey-text text-lighten-4">You can use rows and columns here to organize your footer content.</p>
        </div>
        <div class="col l4 offset-l2 s12">
          <h5 class="white-text">Links</h5>
          <ul>
            <li><a class="grey-text text-lighten-3" href="#!">Link 1</a></li>
            <li><a class="grey-text text-lighten-3" href="#!">Link 2</a></li>
            <li><a class="grey-text text-lighten-3" href="#!">Link 3</a></li>
            <li><a class="grey-text text-lighten-3" href="#!">Link 4</a></li>
          </ul>
        </div>
      </div>
    </div>
    <div class="footer-copyright">
      <div class="container">
      &copy; 2015 Copyright Text
      </div>
    </div>
  </footer>
  <!--Import jQuery before materialize.js-->
  <script type="text/javascript" src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
  <script type="text/javascript" src="{{ asset('/assets/js/materialize.min.js') }}"></script>
</body>
</html>
        